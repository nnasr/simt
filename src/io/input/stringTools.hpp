#ifndef SRC_IO_INPUT_STRINGTOOLS
#define SRC_IO_INPUT_STRINGTOOLS

#include <string>
#include <vector>

/* Splits s using characters from delim.
 *
 *  All trailing and preceeding spaces are removed from the original string,
 *  prior to any operation.
 *  Successive delimiters lead to empty strings being inserted.
 *  Example: stringSplit("   foo ,,, bar ,,  ",",") returns
 *  the std::vector {"foo","","","bar","",""}
 */
std::vector<std::string> stringSplit(std::string s, std::string delim = " \t");

// Removes trailing and preceeding spaces from given string.
std::string stringTrim(std::string);

// Converts eligible characters of input std::string to upper case.
std::string toUpper(std::string);

#endif /* SRC_IO_INPUT_STRINGTOOLS */
