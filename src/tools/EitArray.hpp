#ifndef __EITARRAY_HPP__
#define __EITARRAY_HPP__

#include <array>
#include <cmath>
#include <iostream>

#include "ParEITConfig.hpp"

// template <class _Type, std::size_t _Dim>
// using EitArray = std::array<_Type, _Dim>;

// template <std::size_t _Dim>
// using EitArrayInt = EitArray<int_t, _Dim>;

// template <std::size_t _Dim>
// using EitArrayUInt = EitArray<std::size_t, _Dim>;

// template <std::size_t _Dim>
// using EitArrayReal = EitArray<real_t, _Dim>;

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator+ (const std::array<_Type, _Dim> &a, const std::array<_Type, _Dim> &b)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] += b[i];
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator- (const std::array<_Type, _Dim> &a, const std::array<_Type, _Dim> &b)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] -= b[i];
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator+ (const std::array<_Type, _Dim> &a, const _Type &k)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] += k;
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator+ (const _Type &k, const std::array<_Type, _Dim> &a)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] += k;
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator- (const std::array<_Type, _Dim> &a, const _Type &k)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] -= k;
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator- (const _Type &k, const std::array<_Type, _Dim> &a)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] = k - out[i];
  return out;
}

template <class _Type, std::size_t _Dim>
_Type
dot (const std::array<_Type, _Dim> &a, const std::array<_Type, _Dim> &b)
{
  _Type out = _Type ();
  for (std::size_t i = 0; i < _Dim; ++i)
    out += a[i] * b[i];
  return out;
}

template <class _Type, std::size_t _Dim>
auto
norm (const std::array<_Type, _Dim> &a) -> decltype (std::sqrt (dot (a, a)))
{
  return std::sqrt (dot (a, a));
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator* (const _Type &k, const std::array<_Type, _Dim> &a)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] *= k;
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator* (const std::array<_Type, _Dim> &a, const _Type &k)
{
  return k * a;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator/ (const _Type &k, const std::array<_Type, _Dim> &a)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] = k / out[i];
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator/ (const std::array<_Type, _Dim> &a, const _Type &k)
{
  return a * (1.0 / k);
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator/ (const std::array<_Type, _Dim> &a, const std::array<_Type, _Dim> &b)
{
  std::array<_Type, _Dim> out;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] = a[i] / b[i];
  return out;
}

template <class _Type, std::size_t _Dim>
std::array<_Type, _Dim>
operator* (const std::array<_Type, _Dim> &a, const std::array<_Type, _Dim> &b)
{
  std::array<_Type, _Dim> out = a;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] *= b[i];
  return out;
}

template <class _Type, std::size_t _Dim>
_Type
product (const std::array<_Type, _Dim> &a)
{
  if (_Dim == 0U)
    return _Type (0);

  _Type out = a[0];

  for (std::size_t i = 1U; i < _Dim; ++i)
    out *= a[i];
  return out;
}

template <class _Type, std::size_t _Dim>
std::size_t
indexOfMaxAbs (const std::array<_Type, _Dim> &a)
{
  if (_Dim == 0)
  {
    std::cerr << "can not find the maximum pos" << std::endl;
    MPI_Abort (MPI_COMM_WORLD, -1);
  }

  std::size_t imax = 0U;
  _Type       vmax = std::abs (a[imax]);

  for (uint_t i = 1U; i < _Dim; ++i)
  {
    if (vmax < std::abs (a[i]))
    {
      imax = i;
      vmax = a[i];
    }
  }
  return imax;
}

template <class _Target, class _Type, std::size_t _Dim>
std::array<_Target, _Dim>
convert (const std::array<_Type, _Dim> &a)
{
  std::array<_Target, _Dim> out;
  for (std::size_t i = 0; i < _Dim; ++i)
    out[i] = static_cast<_Target> (a[i]);
  return out;
}

template <class _Type, std::size_t _Dim>
std::ostream &
operator<< (std::ostream &os, const std::array<_Type, _Dim> &a)
{
  os << "(";
  for (uint_t i = 0; i < _Dim - 1; ++i)
    os << a[i] << " ";
  os << a[_Dim - 1] << ")" << std::flush;
  return os;
}

#endif /* __EITARRAY_HPP__ */
