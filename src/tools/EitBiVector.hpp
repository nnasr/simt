#ifndef __EITBIVECTOR_HPP__
#define __EITBIVECTOR_HPP__

#include <algorithm>

#include "ParEITConfig.hpp"

template <class _Type>
class EitBiVector
{
public:
  EitBiVector(uint_t Nneg, uint_t Npos) : m_pos(Npos),
                                          m_neg(Nneg),
                                          m_size(Nneg + Npos),
                                          m_data(nullptr)
  {
    resize(Nneg, Npos);
  }

  EitBiVector(uint_t Nneg, uint_t Npos, const _Type &value) : m_pos(Npos),
                                                              m_neg(Nneg),
                                                              m_size(Nneg + Npos),
                                                              m_data(nullptr)
  {
    resize(Nneg, Npos, value);
  }

  EitBiVector(const EitBiVector &that) : m_pos(that.m_pos),
                                         m_neg(that.m_neg),
                                         m_size(that.m_size),
                                         m_data(nullptr)
  {
    m_data = new _Type[m_size];
    std::copy_n(that.m_data, m_size, m_data);
  }

  EitBiVector &
  operator=(const EitBiVector &that)
  {
    if (this != &that)
    {
      m_pos = that.m_pos;
      m_neg = that.m_neg;
      m_size = m_neg + m_pos;

      delete[] m_data;
      m_data = new _Type[m_size];
      std::copy_n(that.m_data, m_size, m_data);
    }
    return *this;
  }

  ~EitBiVector()
  {
    delete[] m_data;
  }

  _Type &
  operator[](int_t i)
  {
    return m_data[m_neg + i];
  }

  const _Type &
  operator[](int_t i) const
  {
    return m_data[m_neg + i];
  }

  _Type &
  at(int_t i)
  {
    assert((-i <= (int_t)m_neg) and (i < (int_t)m_pos));
    return m_data[m_neg + i];
  }

  const _Type &
  at(int_t i) const
  {
    // assert ((-i <= m_neg) and (i < m_pos));
    return m_data[m_neg + i];
  }

  const uint_t &
  size() const
  {
    return m_size;
  }

  const uint_t &
  posSize() const
  {
    return m_pos;
  }

  const uint_t &
  negSize() const
  {
    return m_neg;
  }

  void
  resize(const uint_t &Nneg, const uint_t &Npos)
  {
    _Type *data = new _Type[Nneg + Npos];

    if (m_data != nullptr)
    {
      _Type *middle = m_data + m_neg;
      _Type *beg = middle - std::min(m_neg, Nneg);
      _Type *end = middle + std::min(m_pos, Npos);
      std::copy(beg, end, data);

      delete[] m_data;
    }

    m_data = data;
    return;
  }

  void
  resize(const uint_t &Nneg, const uint_t &Npos, const _Type &value)
  {
    resize(Nneg, Npos);
    fill(value);
    return;
  }

  _Type *
  data()
  {
    return m_data;
  }

  const _Type *
  data() const
  {
    return m_data;
  }

  _Type *
  dataPos()
  {
    return m_data + m_neg;
  }

  const _Type *
  dataPos() const
  {
    return m_data + m_neg;
  }

  _Type *
  dataNeg()
  {
    return m_data;
  }

  const _Type *
  dataNeg() const
  {
    return m_data;
  }

  EitBiVector &
  operator+=(const EitBiVector &a)
  {
    assert(size() == a.size());
    for (uint_t i = 0U; i < size(); ++i)
      m_data[i] += a.m_data[i];
    return *this;
  }

  EitBiVector &
  operator-=(const EitBiVector &a)
  {
    assert(size() == a.size());
    for (uint_t i = 0U; i < size(); ++i)
      m_data[i] -= a.m_data[i];
    return *this;
  }

  EitBiVector &
  operator*=(const _Type &factor)
  {
    for (uint_t i = 0U; i < size(); ++i)
      m_data[i] *= factor;
    return *this;
  }

  void
  fill(const _Type &v)
  {
    for (uint_t i = 0U; i < size(); ++i)
      m_data[i] = v;
    return;
  }

  void
  zero()
  {
    return fill((_Type)0);
  }

  void
  ones()
  {
    return fill((_Type)1);
  }

protected:
  uint_t m_pos, m_neg, m_size;
  _Type *m_data;
};

template <class _Type>
_Type dot(const _Type *a, const _Type *b, const uint_t &n, bool reduce)
{
  _Type value = _Type();
  for (uint_t i = 0U; i < n; ++i)
    value += a[i] * b[i];
  if (reduce)
    MPI_Allreduce(MPI_IN_PLACE, &value, 1, EitDataType<_Type>::get(), MPI_SUM, MPI_COMM_WORLD);
  return value;
}

template <class _Type>
_Type specialDot(const EitBiVector<_Type> &a, const EitBiVector<_Type> &b)
{
  assert(a.posSize() == b.posSize());
  assert(a.negSize() == b.negSize());

  _Type value = dot(a.dataPos(), b.dataPos(), a.posSize(), true);
  value += dot(a.dataNeg(), b.dataNeg(), a.negSize(), false);

  return value;
}

template <class _Type>
_Type norm(const EitBiVector<_Type> &a)
{
  return std::sqrt(specialDot(a,a));
}

template <class _Type>
EitBiVector<_Type>
operator+(const EitBiVector<_Type> &a, const EitBiVector<_Type> &b)
{
  EitBiVector<_Type> out = a;
  out += b;
  return out;
}

template <class _Type>
EitBiVector<_Type>
operator-(const EitBiVector<_Type> &a, const EitBiVector<_Type> &b)
{
  EitBiVector<_Type> out = a;
  out -= b;
  return out;
}

template <class _Type>
EitBiVector<_Type>
operator*(const _Type &factor, const EitBiVector<_Type> &b)
{
  EitBiVector<_Type> out = b;
  out *= factor;
  return out;
}

template <class _Type>
EitBiVector<_Type>
operator*(const EitBiVector<_Type> &a, const _Type &factor)
{
  return factor * a;
}

#endif /* __EITBIVECTOR_HPP__ */
