#ifndef __EITMATH_HPP__
#define __EITMATH_HPP__

#include <cmath>
#include <functional>

#include "ParEITConfig.hpp"

real_t
myAround(real_t x);

real_t
myDecimal(real_t x);

template <typename T>
int sgn(T val)
{
    return (T(0) < val) - (val < T(0));
}


#endif /* __EITMATH_HPP__ */
